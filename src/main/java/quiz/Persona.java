package quiz;

public class Persona {
    
    //Atributos 
    String cedula;
    String nombre;
    Vehiculo auto;

    public Persona(String cedula, String nombre, Vehiculo auto) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.auto = auto;
    }


    @Override
    public String toString() {
        return "Persona{" + "cedula=" + cedula
                + " nombre=" + nombre 
                + " auto=" + auto + '}';
    }
    
    

}
