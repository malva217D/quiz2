package quiz;

/**
 *
 * @author M@rlon Alvarado
 */
public class Motor {

    //Atributos
    int código;
    int cilindros;
    int caballos_fuerza;

    public Motor(int código, int cilindros, int caballos_fuerza) {
        this.código = código;
        this.cilindros = cilindros;
        this.caballos_fuerza = caballos_fuerza;
    }

    @Override
    public String toString() {
        return "Motor{"+ "c\u00f3digo=" + código 
                + " cilindros=" + cilindros 
                + " caballos_fuerza=" + caballos_fuerza+ '}';
    }

   

   

   
}
