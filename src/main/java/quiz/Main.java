package quiz;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {

        Stack pila = new Stack();
        ArrayList lista = new ArrayList();
        Motor motor1 = new Motor(001, 200, 11);
        Motor motor2 = new Motor(002, 300, 12);
        Motor motor3 = new Motor(003, 400, 13);
        Motor motor4 = new Motor(004, 500, 14);
        Motor motor5 = new Motor(005, 600, 15);
        Vehiculo vehiculo1 = new Vehiculo("Toyota", "2020", "Rojo", motor1);
        Vehiculo vehiculo2 = new Vehiculo("Hyundai", "2010", "Azul", motor2);
        Vehiculo vehiculo3 = new Vehiculo("Honda", "2000", "Blanco", motor3);
        Vehiculo vehiculo4 = new Vehiculo("BMW", "2011", "Amarillo", motor4);
        Vehiculo vehiculo5 = new Vehiculo("Susuki", "2019", "Gris", motor5);
        
        pila.push(vehiculo1);
        pila.push(vehiculo2);
        pila.push(vehiculo3);
        pila.push(vehiculo4);
        pila.push(vehiculo5);

        Persona cliente1 = new Persona("217", "Marlon", vehiculo1);
        Persona cliente2 = new Persona("218", "Melina", vehiculo2);
        Persona cliente3 = new Persona("219", "Mateo", vehiculo3);
        Persona cliente4 = new Persona("220", "Elias", vehiculo4);
        Persona cliente5 = new Persona("221", "Jorge", vehiculo5);

        LinkedList cola = new LinkedList();

        cola.offer(cliente1);
        cola.offer(cliente2);
        cola.offer(cliente3);
        cola.offer(cliente4);
        cola.offer(cliente5);
        for (int i = 0; i < 5; i++) {
            lista.add(cola.poll());
        }
        System.out.println(lista.get(0));
        System.out.println(lista.get(1));
        System.out.println(lista.get(2));
        System.out.println(lista.get(3));
        System.out.println(lista.get(4));
    }

}
