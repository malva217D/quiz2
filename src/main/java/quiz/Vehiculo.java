package quiz;

public class Vehiculo {

    String marca;
    String modelo;
    String color;
    Motor motor;

    public Vehiculo(String marca, String modelo, String color, Motor motor) {
        this.marca = marca;
        this.modelo = modelo;
        this.color = color;
        this.motor = motor;
    }

    @Override
    public String toString() {
        return "Vehiculo{" + "marca=" + marca
                + " color=" + color
                + " motor=" + motor + '}';
    }

}
